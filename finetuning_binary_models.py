  # -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 18:58:52 2020

@author: arthu
"""

from __future__ import print_function, division
import os
from builtins import range, input
import sys  #???
import tensorflow as tf
from tensorflow import keras
#Contruction du CNN
#L'étape de préparation de données se fait manuellement
from tensorflow.keras.preprocessing import image
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.applications import EfficientNetB0
# from tensorflow import set_random_seed
# set_random_seed(0)
from numpy.random import seed
seed(0)
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from glob import glob  #Module permetant de faire une liste de chemin ayant un nom ou une caractéristiques rechercher
from readPreprocess_cluster import read_and_preprocess
from tensorflow.keras.callbacks import Callback, EarlyStopping
from math import exp
from sklearn.metrics import roc_auc_score,balanced_accuracy_score,f1_score
#from tensorflow.keras.utils.np_utils import to_categorical  
from tensorflow.keras.optimizers import Adam
from generate_binary_models_funct import model_gen 
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications.efficientnet import preprocess_input
from backup_cluster import LearningRateScheduler,roc_callback
from tensorflow.compat.v1 import ConfigProto         #resolve the train_function error with Nvidia RTX GPU
from tensorflow.compat.v1 import InteractiveSession
config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)
if tf.test.gpu_device_name(): 
    print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))
else:
    print("Please install GPU version of TF")
gpus = tf.config.experimental.list_physical_devices('GPU')
print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))


  # Restrict TensorFlow to only allocate 4GB of memory on the first GPU
# if gpus:
#     try:
#         tf.config.experimental.set_virtual_device_configuration(
#         gpus[0],
#         [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=6144),
# #          tf.config.experimental.VirtualDeviceConfiguration(memory_limit=2048)
#           ])
#         logical_gpus = tf.config.experimental.list_logical_devices('GPU')
#         print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
#     except RuntimeError as e:
#     # Virtual devices must be set before GPUs have been initialized
#         print(e)
root_path=os.getcwd()
data_root=root_path+'/dataset/'   #end with /
checkpoint_root=root_path+'/checkpoint/'
from finetuning_binary_models_parameters import train_parameters
train_init_name='TRAIN_INIT'
task_list,split_list,cnn_name,model_name,image_size,task_name,hyperparameters,batch_size,epochs,step,state,best_previous_bacc_val,load_w,pursue,task_to_pursue,weights_filename,weights_dict=train_parameters()

if pursue==False:
    for task_name in task_list:
        for split in split_list:
            if 'X_valid2' and 'y_valid2' and 'train_filenames' and 'model' in globals():
                del X_valid2, y_valid2, train_filenames,model
            state=0
            #Importation et préparation des données
            #Direction de la base de données
            train_path = data_root+split+'/'+task_name+'/TRAIN'
            valid_path =data_root+split+'/'+task_name+'/VALID'
            # évalue le nombre d'image dans chaque dossier
            train_set = glob(train_path + '/*/*.JPG')
            valid_set = glob(valid_path + '/*/*.JPG')
            # évalue le nombre de classes
            numClass = glob(train_path + '/*')
            nben=len(glob(train_path+'/'+task_name[0:3]+'/*'))
            nmal=len(glob(train_path + '/*/*.JPG'))-nben
            [X_valid2,y_valid2,train_filenames]=read_and_preprocess(valid_path,image_size,preprocess_input)
            if step==1:
                name_weights=''
                if not os.path.exists(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/cnn_architecture'):
                    os.makedirs(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/cnn_architecture')
                if not os.path.exists(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/best_weigths'):
                    os.makedirs(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/best_weigths')
                if not os.path.exists(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/training_history'):
                    os.makedirs(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/training_history')
                train_path = data_root+split+'/'+task_name+'/'+train_init_name
                train_datagen = ImageDataGenerator(preprocessing_function = preprocess_input)
                train_set = glob(train_path + '/*/*.JPG')
            elif step==2:
                name_weights=weights_dict[task_name][split]
                train_datagen = ImageDataGenerator(preprocessing_function = preprocess_input)

            valid_datagen = ImageDataGenerator(preprocessing_function = preprocess_input)        #pour le jeu de test, permet de normaliser
    #les fonctions suivantes sont celles qui vont véritablement agir sur nos images de base et créer les nouvelles images 
            train_gen = train_datagen.flow_from_directory(train_path,
                                                  target_size = image_size,   #taille des nouvelles images
                                                  shuffle=True, #Permet de mélanger aléatoirement le jeu de donnée permettant ainsi d'améliorer la qualité du modèle et ainsi que ses performances
                                                  batch_size = batch_size,          #mise à jout des poids apres des lots d'observation
                                                  class_mode = 'binary')
            valid_gen = valid_datagen.flow_from_directory(valid_path,
                                            target_size = image_size,
                                            shuffle=False,
                                            batch_size = batch_size,
                                            class_mode = 'binary')
    
    
         
        
            path_history=checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/training_history/'
            path_weights=checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/best_weigths/'
            path_weights_load=path_weights+name_weights+'.hdf5'
            
            for lr,rf in hyperparameters:
                    model=model_gen(rf,path_weights_load,load_w,model_name)
                    state+=1
                    print('curent step:', step)
                    print('curent task:', task_name)
                    print('curent split:', split)
                    print('curent state:', state)
                    if state==1 & step==1:
                        model.save(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/cnn_architecture/'+model_name+'.h5')
                    opt=Adam(learning_rate=lr)
                    model.compile(
                            loss='binary_crossentropy',
                            optimizer=opt,
                            metrics=['accuracy']
                        )
                    #ENTRAINEMENT DU MODELE
                    # training config:
                    wben = (nben+nmal)/nben 
                    wmal = (nben+nmal)/nmal 
                    resultat=model.fit(train_gen,
                             steps_per_epoch=len(train_set)//batch_size,
                             epochs=epochs,
                             verbose=0,
                             validation_data= valid_gen,
                             class_weight={0:wben,1:wmal},
                             #workers=3,
                             validation_steps=len(valid_set)//batch_size,
                             callbacks=[LearningRateScheduler(epo=(epochs,lr,state)),
                                                roc_callback(validation_data=(X_valid2,y_valid2,rf,lr,epochs,state,step,path_history,path_weights,best_previous_bacc_val,model_name,task_name,split))]
                                     )
          
else :
    task_list=task_list[0+task_to_pursue:len(task_list)+1]
    for task_name in task_list:
        for split in split_list:
            if 'X_valid2' and 'y_valid2' and 'train_filenames' and 'model' in globals():
                del X_valid2, y_valid2, train_filenames,model
            state=0
            #Importation et préparation des données
            #Direction de la base de données
            train_path = data_root+split+'/'+task_name+'/TRAIN'
            valid_path =data_root+split+'/'+task_name+'/VALID'
            # train_path = data_root+task_name+'/TRAIN'
            # valid_path =data_root+task_name+'/VALID'
            # évalue le nombre d'image dans chaque dossier
            train_set = glob(train_path + '/*/*.JPG')
            valid_set = glob(valid_path + '/*/*.JPG')
            # évalue le nombre de classes
            numClass = glob(train_path + '/*')
            nben=len(glob(train_path+'/'+task_name[0:3]+'/*'))
            nmal=len(glob(train_path + '/*/*.JPG'))-nben
            [X_valid2,y_valid2,train_filenames]=read_and_preprocess(valid_path,image_size,preprocess_input)
            name_weights=weights_dict[task_name][split]
            if step==1:
                name_weights=''
                if not os.path.exists(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/cnn_architecture'):
                    os.makedirs(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/cnn_architecture')
                if not os.path.exists(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/best_weigths'):
                    os.makedirs(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/best_weigths')
                if not os.path.exists(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/training_history'):
                    os.makedirs(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/training_history')
                train_path = data_root+split+'/'+task_name+'/'+train_init_name
                train_datagen = ImageDataGenerator(preprocessing_function = preprocess_input)
                train_set = glob(train_path + '/*/*.JPG')
            elif step==2:
                name_weights=weights_dict[task_name][split]
                train_datagen = ImageDataGenerator(preprocessing_function = preprocess_input)

            valid_datagen = ImageDataGenerator(preprocessing_function = preprocess_input)        #pour le jeu de test, permet de normaliser
    #les fonctions suivantes sont celles qui vont véritablement agir sur nos images de base et créer les nouvelles images 
            train_gen = train_datagen.flow_from_directory(train_path,
                                                  target_size = image_size,   #taille des nouvelles images
                                                  shuffle=True, #Permet de mélanger aléatoirement le jeu de donnée permettant ainsi d'améliorer la qualité du modèle et ainsi que ses performances
                                                  batch_size = batch_size,          #mise à jout des poids apres des lots d'observation
                                                  class_mode = 'binary')
            valid_gen = valid_datagen.flow_from_directory(valid_path,
                                            target_size = image_size,
                                            shuffle=True,
                                            batch_size = batch_size,
                                            class_mode = 'binary')    
            path_history=checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/training_history/'
            path_weights=checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/best_weigths/'
            path_weights_load=path_weights+name_weights+'.hdf5'
            
            for lr,rf in hyperparameters:
                    model=model_gen(rf,path_weights_load,load_w,model_name)
                    state+=1
                    print('curent step:', step)
                    print('curent task:', task_name)
                    print('curent split:', split)
                    print('curent state:', state)
                    if state==1 & step==1:
                        model.save(checkpoint_root+split+'/'+cnn_name+'/'+task_name+'/cnn_architecture/'+model_name+'.h5')
                    opt=Adam(learning_rate=lr)
                    model.compile(
                            loss='binary_crossentropy',
                            optimizer=opt,
                            metrics=['accuracy']
                        )
                    #ENTRAINEMENT DU MODELE
                    # training config:
                    wben = (nben+nmal)/nben 
                    wmal = (nben+nmal)/nmal 
                    resultat=model.fit(train_gen,
                             steps_per_epoch=len(train_set)//batch_size,
                             epochs=epochs,
                             verbose=0,
                             validation_data= valid_gen,
                             class_weight={0:wben,1:wmal},
                             #workers=3,
                             validation_steps=len(valid_set)//batch_size,
                             callbacks=[LearningRateScheduler(epo=(epochs,lr,state)),
                                                roc_callback(validation_data=(X_valid2,y_valid2,rf,lr,epochs,state,step,path_history,path_weights,best_previous_bacc_val,model_name,task_name,split))]
                                     )
    

