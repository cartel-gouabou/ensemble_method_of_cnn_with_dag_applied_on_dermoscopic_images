import pandas as pd
def train_parameters():
    task_list=['bekVSmel','bekVSnev','melVSnev'] #

   
    split_list=['CV1','CV2','CV3']
  
    cnn_name='VGG19' # architecture VGG16 or VGG19 or ResNet50
    model_name='VGG19' # architecture VGG16 or VGG19 or ResNet50
    image_size = [224,224]  # size B0

    task_name=''
    hyperparameters=[(0.01,4),(0.01,8),(0.01,16),(0.01,32),(0.01,64),(0.001,4),(0.001,8),(0.001,16),(0.001,32),(0.001,64),(0.0001,4),(0.0001,8),(0.0001,16),(0.0001,32),(0.0001,64)]
    batch_size = 32 
    epochs =150
    step=1 #1 to initialize last layer and 2 to finetune all the models
    state=0
    best_previous_bacc_val=60 #inf if step=1
    load_w=False
    pursue=False
    task_to_pursue=0
    weights_filename=''
    weights_dict=[]
    name_W_ACK=['','','','','']
    name_W_BCC=['','','','','']
    name_W_BEK=['','','','','']
    name_W_DEF=['','','','','']
    name_W_MEL=['','','','','']
    name_W_NEV=['','','','','']
    name_W_SCC=['','','','','']
    name_W_VAL=['','','','','']
    weights_dict = pd.DataFrame(list(zip(name_W_ACK,name_W_BCC)), 
                                index =split_list,
                                columns =task_list)
    return task_list,split_list,cnn_name,model_name,image_size,task_name,hyperparameters,batch_size,epochs,step,state,best_previous_bacc_val,load_w,pursue,task_to_pursue,weights_filename,weights_dict