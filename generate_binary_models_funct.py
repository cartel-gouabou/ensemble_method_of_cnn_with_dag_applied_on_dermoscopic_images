# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 17:41:19 2020

@author: arthu
"""
#ration_freeze :percentage of layer to finetune
#w type of weight initialisation 1 for imagenet and 0 for random
#

def model_gen(ratio_freeze,path_weights,load,cnn_name):
    from tensorflow.keras.layers import Input, Dropout, Dense, GlobalAveragePooling2D
    from tensorflow.keras.layers.experimental.preprocessing import Rescaling
    from tensorflow.keras.models import Model,load_model
    from tensorflow.keras import initializers
    from keras.applications.vgg16 import VGG16
    from keras.applications.vgg19 import VGG19
    from keras.applications.resnet import ResNet50
    if cnn_name =='VGG16':
        from keras.applications.vgg16 import VGG16
        model_O=VGG16(input_shape=(224, 224, 3),weights='imagenet',include_top=True) #'weights=imagenet'
        num_layer=len(model_O.layers)
        num_freeze_layer= (ratio_freeze*num_layer)//100
        model_O.layers.pop()
        prediction=model_O.layers[-2].output
        prediction = Dense(2, kernel_initializer='random_normal',activation='softmax')(prediction)
        model = Model(inputs=model_O.input, outputs=prediction)
        for layer in model.layers:
             layer.trainable = True
        for layer in model.layers[:-num_freeze_layer]:
            layer.trainable = False   
        if load==True:
            model.load_weights(path_weights) 
        return model
    elif cnn_name == 'VGG19':
        from keras.applications.vgg19 import VGG19
        model_O=VGG19(input_shape=(240, 240, 3),weights='imagenet',include_top=True) #'weights=imagenet'
        num_layer=len(model_O.layers)
        num_freeze_layer= (ratio_freeze*num_layer)//100
        model_O.layers.pop()
        prediction=model_O.layers[-2].output
        prediction = Dense(1, kernel_initializer='random_normal',activation='softmax')(prediction)
        model = Model(inputs=model_O.input, outputs=prediction)
        for layer in model.layers:
             layer.trainable = True
        for layer in model.layers[:-num_freeze_layer]:
            layer.trainable = False   
        if load==True:
            model.load_weights(path_weights) 
        return model
    elif cnn_name == 'ResNet50':
        from keras.applications.resnet import ResNet50
        model_O=ResNet50(input_shape=(224, 224, 3),weights='imagenet',include_top=True) #'weights=imagenet'
        num_layer=len(model_O.layers)
        num_freeze_layer= (ratio_freeze*num_layer)//100
        model_O.layers.pop()
        prediction=model_O.layers[-2].output
        prediction = Dense(1, kernel_initializer='random_normal',activation='softmax')(prediction)
        model = Model(inputs=model_O.input, outputs=prediction)
        for layer in model.layers:
             layer.trainable = True
        for layer in model.layers[:-num_freeze_layer]:
            layer.trainable = False   
        if load==True:
            model.load_weights(path_weights) 
        return model
    else:
        print('Error: specified model name')
    
